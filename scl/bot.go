package scl

import (
	"bitbucket.org/aisee/minilog"
	"bitbucket.org/aisee/sc2lib/actions"
	"bitbucket.org/aisee/sc2lib/grid"
	"bitbucket.org/aisee/sc2lib/point"
	"bitbucket.org/aisee/sc2lib/tests"
	"fmt"
	"github.com/chippydip/go-sc2ai/api"
	"github.com/chippydip/go-sc2ai/client"
	"github.com/chippydip/go-sc2ai/enums/terran"
	"math"
	"os"
)

type Bot struct {
	Info          client.AgentInfo
	Obs           *api.Observation
	Actions       actions.Actions
	Cmds          *CommandsStack
	DebugCommands []*api.DebugCommand

	Locs struct {
		MapCenter       point.Point
		MyStart         point.Point
		MyStartMinVec   point.Point
		EnemyStart      point.Point
		EnemyStarts     point.Points
		EnemyMainCenter point.Point
		MyExps          point.Points
		EnemyExps       point.Points
	}
	Ramps struct {
		All   []Ramp
		My    Ramp
		Enemy Ramp
	}
	Units struct {
		My       UnitsByTypes
		Enemy    UnitsByTypes
		AllEnemy UnitsByTypes
		Minerals UnitsByTypes
		Geysers  UnitsByTypes
		Neutral  UnitsByTypes
	}
	Enemies struct {
		All      Units
		AllReady Units
		Visible  Units
		Clusters []*Cluster
	}

	Grid           *grid.Grid
	SafeGrid       *grid.Grid
	ReaperGrid     *grid.Grid
	ReaperSafeGrid *grid.Grid
	// HomePaths        Steps
	// HomeReaperPaths  Steps
	// ExpPaths         []Steps
	WayMap           WaypointsMap
	SafeWayMap       WaypointsMap
	ReaperWayMap     WaypointsMap
	ReaperSafeWayMap WaypointsMap

	EnemyRace       api.Race
	EnemyProduction TagsByTypes
	Orders          map[api.AbilityID]int
	FramesPerOrder  int
	Groups          *Groups
	MaxGroup        GroupID
	Upgrades        map[api.AbilityID]bool

	Loop             int
	LastLoop         int
	Minerals         int
	MineralsPerFrame float64
	Vespene          int
	VespenePerFrame  float64
	FoodCap          int
	FoodUsed         int
	FoodLeft         int

	UnitCreatedCallback func(unit *Unit)
}

const FPS = 22.4
const HitHistoryLoops = 56 // 2.5 sec
const ResourceSpreadDistance = 9
const minRampSize = 10
const airSpeedBoostRange = 5
const samePoint = 0.1
const KD8Radius = 1.75 // todo: exact data

func (b *Bot) Init() { // todo: Init -> New
	InitUnits(b.Info.Data().Units)
	InitUpgrades(b.Info.Data().Upgrades)
	InitEffects(b.Info.Data().Effects)
	b.InitLocations()
	b.FindExpansions()
	b.InitMining()
	b.FindRamps()
	b.InitRamps()
	go b.RenewPaths()
}

func (b *Bot) CanBuy(ability api.AbilityID) bool {
	cost, ok := AbilityCost[ability]
	if !ok {
		log.Warning("no cost for ability: ", ability)
	}
	return (cost.Minerals == 0 || b.Minerals >= cost.Minerals) &&
		(cost.Vespene == 0 || b.Vespene >= cost.Vespene) &&
		(cost.Food <= 0 || b.FoodLeft >= cost.Food)
}

func (b *Bot) DeductResources(aid api.AbilityID) {
	cost := AbilityCost[aid]
	b.Minerals -= cost.Minerals
	b.Vespene -= cost.Vespene
	if cost.Food > 0 {
		b.FoodUsed += cost.Food
		b.FoodLeft -= cost.Food
	}
}

func (b *Bot) Pending(aid api.AbilityID) int {
	return b.Units.My[AbilityUnit[aid]].Len() + b.Orders[aid]
}

func (b *Bot) PendingAliases(aid api.AbilityID) int {
	return b.Units.My.OfType(UnitAliases.For(AbilityUnit[aid])...).Len() + b.Orders[aid]
}

func (b *Bot) CanBuild(aid api.AbilityID, limit, active int) bool {
	return b.CanBuy(aid) && b.Pending(aid) < limit && b.Orders[aid] < active
}

func (b *Bot) AddToCluster(enemy *Unit, c *Cluster) {
	c.Units[enemy] = struct{}{}
	c.Food += float64(Types[enemy.UnitType].FoodRequired)
	enemy.Cluster = c

	for _, u := range enemy.Neighbours {
		if u.Cluster != nil {
			continue
		}

		b.AddToCluster(u, c)
	}
}

func (b *Bot) FindClusters() {
	enemies := b.Enemies.AllReady.Filter(func(unit *Unit) bool {
		return !unit.IsWorker() && (unit.IsDefensive() || !unit.IsStructure())
	})
	/*enemies := b.Units.My.All().Filter(func(unit *Unit) bool {
		return unit.IsReady() && !unit.IsWorker() && (unit.IsDefensive() || !unit.IsStructure())
	})*/
	// Find neighbours for each unit
	for _, u := range enemies {
		u.Neighbours = enemies.Filter(func(unit *Unit) bool {
			if u == unit {
				return false
			}

			r1 := math.Max(u.GroundRange(), u.AirRange())
			r2 := math.Max(unit.GroundRange(), unit.AirRange())
			r := math.Max(r1, r2) + 2
			return unit.IsCloserThan(r, u)
		})
	}

	// Add units connected by neighbourship to clusters
	b.Enemies.Clusters = []*Cluster{}
	for _, enemy := range enemies {
		enemy.Cluster = nil // Remove cluster for old enemies (not visible now)
	}
	for _, enemy := range enemies {
		if enemy.Cluster != nil {
			continue
		}

		c := &Cluster{Units: UnitsMap{}}
		b.AddToCluster(enemy, c)
		b.Enemies.Clusters = append(b.Enemies.Clusters, c)
	}
}

func (b *Bot) ParseUnits() {
	// Restore default data
	if b.Grid == nil {
		b.Grid = grid.New(b.Info.GameInfo().StartRaw, b.Info.Observation().Observation.RawData.MapState)
	} else {
		// I need to renew it because it could be locked somewhere else
		b.Grid.Renew(b.Info.GameInfo().StartRaw, b.Info.Observation().Observation.RawData.MapState)
	}
	b.Grid.Lock()

	b.Units.My = UnitsByTypes{}
	b.Units.Minerals = UnitsByTypes{}
	b.Units.Geysers = UnitsByTypes{}
	b.Units.Neutral = UnitsByTypes{}
	b.Units.Enemy = UnitsByTypes{}
	if b.Groups == nil {
		b.Groups = NewGroups(b.MaxGroup)
	} else {
		b.Groups.ClearUnits()
	}
	if b.Units.AllEnemy == nil {
		b.Units.AllEnemy = UnitsByTypes{}
	}
	oldEnemyUnits := b.Units.AllEnemy.All()
	b.Units.AllEnemy = UnitsByTypes{}
	visibleTags := map[api.UnitTag]bool{}

	for _, unit := range b.Info.Observation().Observation.RawData.Units {
		u, isNew := b.NewUnit(unit)
		switch unit.Alliance {
		case api.Alliance_Self:
			b.Units.My.Add(unit.UnitType, u)
			b.Groups.Fill(u)
			if isNew && b.UnitCreatedCallback != nil {
				b.UnitCreatedCallback(u)
			}
		case api.Alliance_Enemy:
			b.Units.Enemy.Add(unit.UnitType, u)
			b.Units.AllEnemy.Add(unit.UnitType, u)
			visibleTags[u.Tag] = true
			b.EnemyProduction.Add(unit.UnitType, unit.Tag)
		case api.Alliance_Neutral:
			if u.IsMineral() {
				b.Units.Minerals.Add(unit.UnitType, u)
			} else if u.IsGeyser() { // todo: filter empty
				b.Units.Geysers.Add(unit.UnitType, u)
			} else {
				b.Units.Neutral.Add(unit.UnitType, u)
			}
		default:
			fmt.Fprintln(os.Stderr, "Not supported alliance: ", unit)
			continue
		}

		// Modify pathing and building maps
		if u.IsStructure() && !u.IsFlying {
			if u.Alliance == api.Alliance_Self || u.Alliance == api.Alliance_Enemy {
				var size BuildingSize = 0
				pos := u.Point()
				switch {
				case u.Radius <= 1:
					// Nothing
				case u.Radius >= 1.125 && u.Radius <= 1.25:
					size = S2x2
					pos -= point.Pt(1, 1)
				case u.Radius > 1.25 && u.Radius < 2.75:
					size = S3x3
				case u.Radius == 2.75:
					size = S5x5
				default:
					log.Notice("No size for building:", u.UnitType, u.Radius)
				}
				if size != 0 {
					for _, p := range b.GetBuildingPoints(pos, size) {
						b.Grid.SetBuildable(p, false)
						if u.UnitType != terran.SupplyDepotLowered {
							b.Grid.SetPathable(p, false)
						}
					}
				}
			} else { // api.Alliance_Neutral
				// todo: correct sizes instead of copypaste
				var size BuildingSize = 0
				pos := u.Point()
				switch {
				case u.Radius <= 1:
					// Nothing
				case u.Radius >= 1.125 && u.Radius <= 1.25:
					size = S2x2
					pos -= point.Pt(1, 1)
					/*case u.Radius > 1.25 && u.Radius < 2.75:
						size = S3x3
					case u.Radius == 2.75:
						size = S5x5*/
				default:
					// log.Notice("No size for building:", u.UnitType, u.Radius)
				}
				if size != 0 {
					for _, p := range b.GetBuildingPoints(pos, size) {
						b.Grid.SetBuildable(p, false)
					}
				}
			}
		}
	}
	b.Grid.Unlock()

	for _, u := range oldEnemyUnits {
		visible := true
		h := b.Grid.HeightAt(u)
		// Iterate unit's position and points around it
		for _, p := range append([]point.Point{u.Point()}, u.Point().Neighbours4(1)...) {
			if !b.Grid.IsVisible(p) && b.Grid.HeightAt(p) == h && b.Grid.IsPathable(p) {
				visible = false
				break
			}
		}
		// If unit already added or it's old position is scouted, skip it
		if visibleTags[u.Tag] || visible {
			continue
		}
		u.DisplayType = api.DisplayType_Snapshot
		b.Units.AllEnemy.Add(u.UnitType, u)
	}

	b.Enemies.All = b.Units.AllEnemy.All()
	b.Enemies.AllReady = b.Enemies.All.Filter(Ready)
	b.Enemies.Visible = b.Units.Enemy.All()

	b.RequestAvailableAbilities(false, b.Units.My.All()...)
	b.RequestAvailableAbilities(true, b.Units.My.All()...)
}

func (b *Bot) ParseOrders() {
	b.Orders = map[api.AbilityID]int{}
	for _, unit := range b.Units.My.All() {
		for _, order := range unit.Orders {
			b.Orders[order.AbilityId]++
		}
	}
}

func (b *Bot) ParseObservation() {
	b.Loop = int(b.Obs.GameLoop)
	b.Minerals = int(b.Obs.PlayerCommon.Minerals)
	b.Vespene = int(b.Obs.PlayerCommon.Vespene)
	b.FoodCap = int(b.Obs.PlayerCommon.FoodCap)
	b.FoodUsed = int(b.Obs.PlayerCommon.FoodUsed)
	b.FoodLeft = b.FoodCap - b.FoodUsed
	// todo: check, there should be 22.4 or 16?
	b.MineralsPerFrame = float64(b.Obs.Score.ScoreDetails.CollectionRateMinerals) / 60 / 22.4
	b.VespenePerFrame = float64(b.Obs.Score.ScoreDetails.CollectionRateVespene) / 60 / 22.4
	b.Upgrades = map[api.AbilityID]bool{}
	if Upgrades != nil {
		for _, uid := range b.Obs.RawData.Player.UpgradeIds {
			b.Upgrades[Upgrades[uid].AbilityId] = true
		}
	}
}

func (b *Bot) DetectEnemyRace() {
	if b.EnemyRace == 0 {
		enemyId := 3 - b.Info.PlayerID() // hack?
		b.EnemyRace = b.Info.GameInfo().PlayerInfo[enemyId-1].RaceRequested
	} else if b.EnemyRace == api.Race_Random && b.Units.Enemy.Exists() {
		unit := b.Enemies.Visible.First()
		b.EnemyRace = Types[unit.UnitType].Race
	}
}

func (b *Bot) UnitTargetPos(u *Unit) point.Point {
	pos := u.TargetPos()
	if pos != 0 {
		return pos
	}
	enemy := b.Enemies.Visible.ByTag(u.TargetTag())
	if enemy != nil {
		return enemy.Point()
	}
	return 0
}

func (b *Bot) RequestAvailableAbilities(irr bool, us ...*Unit) {
	var rqaas []*api.RequestQueryAvailableAbilities
	for _, u := range us {
		rqaas = append(rqaas, &api.RequestQueryAvailableAbilities{UnitTag: u.Tag})
	}
	resp := b.Info.Query(api.RequestQuery{Abilities: rqaas, IgnoreResourceRequirements: irr})
	amap := map[api.UnitTag][]api.AbilityID{}
	for _, rqaa := range resp.Abilities {
		for _, aa := range rqaa.Abilities {
			as := amap[rqaa.UnitTag]
			as = append(as, api.AbilityID(aa.AbilityId))
			amap[rqaa.UnitTag] = as
		}
	}
	for _, u := range us {
		if irr {
			u.TrueAbilities = amap[u.Tag]
		} else {
			u.Abilities = amap[u.Tag]
		}
	}
}

func (b *Bot) SaveState() {
	log.Info("Saving state")
	if _, err := os.Stat("data"); os.IsNotExist(err) {
		if err := os.Mkdir("data", 755); err != nil {
			log.Fatal(err)
		}
		if _, err := os.Stat("data/state"); os.IsNotExist(err) {
			if err := os.Mkdir("data/state", 755); err != nil {
				log.Fatal(err)
			}
		}
	}

	obs, _ := b.Info.Observation().Marshal()
	data, _ := b.Info.Data().Marshal()
	info, _ := b.Info.GameInfo().Marshal()
	for file, bytes := range map[string][]byte{
		"observation": obs,
		"data":        data,
		"info":        info,
	} {
		fileName := "data/state/" + file + ".bin"
		f, err := os.Create(fileName)
		if err != nil {
			log.Fatal(err)
		}
		f.Write(bytes)
		f.Close()
	}
}

func (b *Bot) LoadState() {
	info := &tests.AgentInfo{}
	info.LoadObservation("data/state/observation.bin")
	info.LoadData("data/state/data.bin")
	info.LoadInfo("data/state/info.bin")
	b.Info = info
}
