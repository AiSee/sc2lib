package actions

import "github.com/chippydip/go-sc2ai/api"

type Actions []*api.Action

func (a *Actions) ChatSend(msg string) {
	*a = append(*a, &api.Action{
		ActionChat: &api.ActionChat{
			Channel: api.ActionChat_Broadcast,
			Message: msg,
		},
	})
}
